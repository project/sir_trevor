<?php

namespace Drupal\Tests\sir_trevor\Unit\TestDoubles;

use Drupal\Core\Config\ImmutableConfig;

class ImmutableConfigMock extends ImmutableConfig {

  /**
   * ImmutableConfigMock constructor.
   */
  public function __construct() {
  }
}
